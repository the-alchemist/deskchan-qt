# README #

Виджет Deskchan - виртуальный помощник в виде персонажа японской мультипликации.

## Что он может ##
* Общаться готовыми фразами
* Подгружать эти фразы из гуглоспредшита
* Жрать ОЗУ

## Что он должен будет делать ##
* Использоваться в качестве визуального мониторинга того или иного параметра.